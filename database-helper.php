<?php

/*
 * Handlers - su varijable koje drze vrednost nakon return
 * vrednosti u odredjenim metodama
 * 
 * Handlers sluze za to da se ne ponavljamo vise puta u kodu
 * 
 */

class Pdocon {

//kredencijali
    private $host = "localhost";
    private $user = "root";
    private $pass = "";
    private $dbname = "cus_app";
//handle connection
    private $dbh;
//handle errors
    private $errmsg;
//Statement Handler
    private $stmt;

//method to open connection
//$dsn = 'mysql:dbname=testdb;host=127.0.0.1';


    public function __construct() {
        $dsn = "mysql:host=" . $this->host . "; dbname=" . $this->dbname;

        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );

        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
            echo 'Successfully Connected test 2';
        } catch (PDOException $error) {
            $this->errmsg = $error->getMessage();
            echo $this->errmsg;
        }
    }

//Helper functions - ovo su funkcije koje nam pomazu da se ne ponavljamo
//
//
//ako hocemo da napisemo query onda cemo da pozovemo prvo
//connection handler sto je dbh var i na to primenimo 
//funkciju prepere u koju pisemo query
//$this->dbh->prepere("SELECT * FROM users");
//
//da ne bismo svaki put radili isto napravicemo funkciju
    //Write query helper function using the stat property
    public function query($query) {
//        $this->stmt = $this->dbh->prepere("SELECT * FROM users");
        $this->stmt = $this->dbh->prepere($query);
        //$query varijabla ce dobiti vrednost kada je pozovemo kroz argument
    }

    //Creating a bind function helper
    public function bindValue($parm, $value, $type) {
        $this->stmt->bindValue($parm, $value, $type);
    }

    //Function to execute statement
    public function execute() {
        return $this->stmt->execute();
    }

    //Function to check if statement successfully executed
    public function confirm_result() {
//        return $this->stmt->lastInsertId;
        //posto je lastInsertId metod iz PDO Class, ne mozemo staviti $this->stmt sto je 
        //PDOStatement nego $this->dbh sto je PDO jer je samo konektovan na bazu
        //i jos nije vracen true sto bi automatski napravilo PDOStatement objekat ili false sto pravi PDOException objekat
        return $this->dbh->lastInsertId;
    }

    //Command to fetch data in a result set in associative array
    public function fetchMultiple() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function fetchSingle() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

}


