<?php

/*
 * https://www.php.net/manual/en/language.exceptions.php
https://www.php.net/manual/en/language.exceptions.extending.php
 * 
 * class Exception extends Throwable
{
    protected $message = 'Unknown exception';   // exception message
    private   $string;                          // __toString cache
    protected $code = 0;                        // user defined exception code
    protected $file;                            // source filename of exception
    protected $line;                            // source line of exception
    private   $trace;                           // backtrace
    private   $previous;                        // previous exception if nested exception

    public function __construct($message = null, $code = 0, Exception $previous = null);

    final private function __clone();           // Inhibits cloning of exceptions.

    final public  function getMessage();        // message of exception
    final public  function getCode();           // code of exception
    final public  function getFile();           // source filename
    final public  function getLine();           // source line
    final public  function getTrace();          // an array of the backtrace()
    final public  function getPrevious();       // previous exception
    final public  function getTraceAsString();  // formatted string of trace

    // Overrideable
    public function __toString();               // formatted string for display
}
 * 
 * Exception je klasa
 */

Class Cars {

//properties
    public $name = "BMW";
    static public $speed = 400;
    public $user = "Curry";
    public $type = "4WD";
    static private $minpasswordlengt = 8;

//Methods
    function desc() {
        echo 'My car is ' . $this->name . "KMPH and it is a " . $this->type . ".";
    }

    static public function checkspeed($check_speed) {
        if ($check_speed <= self::$speed) {
            echo 'Your car is very fast ';
        } else {
            echo 'Your car is not that fast ';
        }
    }

    
    public function __construct(){
        echo 'hi ' . $this->user . ' You are welcome<br> ';
    }
    //Destruct metod nemaju imena
    public function __destruct() {
        echo '<br>Goodbye ' . $this->name;
    }
}

class Futurcars extends Cars {

    public $type = "Trucks";
    public $name = "Ford";

    function dexcfuturcar() {
        parent::desc();
    }

}
/*
class Exception{
    //properties
    
    //Methods
    public function __construct() {
        ;
    }
    
    public function getMessage(){
        
    }
}

$errormsg = new Exception;
echo $errormsg->getMessage() ;
 * 
 */
//upotreba Exception Handler
//po manuelu koncept exception error message ide ovako
//ovo je prakticno if i else a koncept je kao primer iznad
//ovde ne mora da se praavi instanca radi blindly 

//ovo koristimo recimo kod uspostavljanja konekcije na DB
try{
    throw new Exception("Some error message");
}catch(Exception $e){
    echo $e->getMessage();
}

//KADA KORISTIMO PDO 

try{
    throw new Exception("Some error message");
}catch(PDOException $e){
    echo $e->getMessage();
}
