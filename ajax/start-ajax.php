<?php
/*

 * AJAX is a developer's dream, because you can:

Update a web page without reloading the page
Request data from a server - after the page has loaded
Receive data from a server - after the page has loaded
Send data to a server - in the background
 *  */
/* https://www.w3schools.com/js/js_ajax_intro.asp
 * 
 * What is AJAX?
AJAX = Asynchronous JavaScript And XML.

AJAX is not a programming language.

AJAX just uses a combination of:

A browser built-in XMLHttpRequest object (to request data from a web server)
JavaScript and HTML DOM (to display or use the data)
AJAX is a misleading name. AJAX applications might use XML to transport data, but it is equally common to transport data as plain text or JSON text.

AJAX allows web pages to be updated asynchronously by exchanging data with a web server behind the scenes. This means that it is possible to update parts of a web page, without reloading the whole page.

HOW AJAX WORKS

1. An event occurs in a web page (the page is loaded, a button is clicked)
2. An XMLHttpRequest object is created by JavaScript
3. The XMLHttpRequest object sends a request to a web server
4. The server processes the request
5. The server sends a response back to the web page
6. The response is read by JavaScript
7. Proper action (like page update) is performed by JavaScript

WHEN IT IS USED

To post or load /get /request data from a server-script like PHP

$ajax = new XMLHttpRequest(); --- PHP
var ajax = new XMLHttpRequest()  --- JavaScript

S`obzirom da je XMLHttpRequest objekat, mozemo reci da je AJAX objekat instanciran od XMLHttpRequest klase

example:
 */
?>
<script>
function showHInt(str){
    if(str.length == 0){
        document.getElementById("txtHint").innerHTML = "";
        /*kada se napise return onda vraca valjda ceo taj objekat ovo gore definisano*/
            return;
    }else{
        var xmlhttp.onreadystatechange = function(){
            if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET", "gethint.php?q=" + str, true);
        xmlhttp.send();
    }
}
/* AJAX je ustvari property of JQuery jQuery.ajax()
 * u isto vreme je objekat od roditeljske klase XMLHttpRequest();
 * to mozemo objasniti na sledecem primeru
 * 
 * XMLHttpRequest() je klasa iz JavaScript
 * JavaScript je nastala od Jave i namenjena je za web
 * dok je java namenjena za softvere
 * sintaxa im je ista sve ostalo nije
 * */
class ajax() extends XMLHttpRequest(){
    //properties
    
    //methods
    
    $.ajax()
    //We gonna use this to fetch data from the database with the help of PHP
    
    $.post()
    //We gonna use this to send data to database with the help of PHP
    
    serialize()
    //Collect and Encodes all form data and attach to the $.post
    //method and send to PHP for processing
}

</script>


