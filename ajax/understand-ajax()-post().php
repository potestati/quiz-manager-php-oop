<?php
/*
 * AJAX METODS
 * https://www.w3schools.com/jquery/jquery_ref_ajax.asp
 * $.ajax({name:value, name:value, ... })
 * The parameters specifies one or more name/value pairs for the AJAX request.

  Possible names/values in the table below:

  //imena parametara
 * Name	--- Value/Description

 * async	--- 	A Boolean value indicating whether the request should be handled asynchronous or not. Default is true

 * beforeSend(xhr)	--- 	A function to run before the request is sent

 * cache	--- 	A Boolean value indicating whether the browser should cache the requested pages. Default is true

 * complete(xhr,status)	--- 	A function to run when the request is finished (after success and error functions)

 * contentType	--- 	The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"

 * context	--- 	Specifies the "this" value for all AJAX related callback functions

 * data	--- 	Specifies data to be sent to the server

 * dataFilter(data,type)	--- 	A function used to handle the raw response data of the XMLHttpRequest

 * dataType	--- 	The data type expected of the server response.

 * error(xhr,status,error)	--- 	A function to run if the request fails.

 * global	--- 	A Boolean value specifying whether or not to trigger global AJAX event handles for the request. Default is true

 * ifModified	--- 	A Boolean value specifying whether a request is only successful if the response has changed since the last request. Default is: false.

 * jsonp	--- 	A string overriding the callback function in a jsonp request

 * jsonpCallback	--- 	Specifies a name for the callback function in a jsonp request

 * password	--- 	Specifies a password to be used in an HTTP access authentication request.

 * processData	--- 	A Boolean value specifying whether or not data sent with the request should be transformed into a query string. Default is true

 * scriptCharset	--- 	Specifies the charset for the request

 * success(result,status,xhr)	--- 	A function to be run when the request succeeds

 * timeout	--- 	The local timeout (in milliseconds) for the request

 * traditional	--- 	A Boolean value specifying whether or not to use the traditional style of param serialization

 * type	--- 	Specifies the type of request. (GET or POST)

 * url	--- 	Specifies the URL to send the request to. Default is the current page

 * username	--- 	Specifies a username to be used in an HTTP access authentication request

 * xhr	--- 	A function used for creating the XMLHttpRequest object
 * 
 */

//examples
/* Use the async setting
  How to use the async setting to specify a synchronous request
 */
?>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script>
            $(document).ready(function () {
                $("button").click(function () {
                    $.ajax({url: "demo_ajax_load.txt", async: false, success: function (result) {
                            $("div").html(result);
                        }});
                });
            });
        </script>
    </head>
    <body>

        <div><h2>Let AJAX change this text</h2></div>

        <button>Change Content</button>
        <script>
            $(document).ready(function () {
                $("button").click(function () {
                    $.ajax({url: "demo_ajax_script.js", dataType: "script"});
                });
            });
        </script>
    </head>
<body>

    <button>Use Ajax to get and then run a JavaScript</button>
    <script>
        $(document).ready(function () {
            $("button").click(function () {
                $.ajax({url: "wrongfile.txt", error: function (xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }});
            });
        });
    </script>
</head>
<body>

    <p>Artists</p>
    <div></div>

    <button>Get CD info</button>


    <script>
        //neki primeri syntax-e
        $.ajax({key: value, key: value, key: value})
        //to je ustvari
        $.ajax({ime_parametra: value, ime_parametra: value})

        url: '', //the php file that will process the data (action)
        //action je u formi action atribut gde treba taj podatak da se posalje
        type: '', //the a is how your want the data to be send (method)
        //tip metoda za slanje get ili post opet iz forme atribut
    data: '', //the values that you want to send with the request to the processing file php file for example
       //podaci koje saljemo 

        sucess: '', //the php file that will process the data (action)
    //result, status, xhr
    
    //ajax ne moze da se koristi da uzme podatak iz baze direktno vec se to radi preko php-a
    //ide PHP ili js - ajax - PHP - SQL
    
    //.post() je da upisemo podatke u bazu uz pomoc php
    //https://www.w3schools.com/jquery/ajax_post.asp
    $('#selector').post(URL, data, function(data, status, xhr), dataType)
    //xhr - sadrzi XMLHttpRequest objekt
    
    $.post(url, data, function(alert){
        alert('Update success');
    })
    </script>
</body>
</html>




