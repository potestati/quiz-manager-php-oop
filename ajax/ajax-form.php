<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <br><br><br>
            <div id="greetings" class="well well-sm text-center container"> </div>
            <h2>Customers</h2>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <table class="table" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Full Name</th>
                                            <th>Password</th>
                                            <th>Email</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'pdocon.php';
                                        $db = new Pdocon;
                                        $db->query("SELECT * FROM users");
                                        $results = $db->fetchMultiple();
//                               echo gettype($result);
                                        foreach ($results as $result) :
                                            ?>
                                            <tr>
                                                <td><a href="#"><?php echo $result['full_name']; ?></a></td>
                                                <td><?php echo $result['password']; ?></td>
                                                <td><?php echo $result['email']; ?></td>
                                                <td><a class="btn btn-primary" href="update.php?user_id=<?php echo $result['id']; ?>">Edit</a></td>
                                            </tr>
                                        <?php endforeach; ?>  
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form action="">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="remember"> Remember me
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <script>
            //sending request
            //ovoje prvo sto napisemo kada radimo sa query
//                $(document).ready(function(){
//                    
//                })
//to znaci pusti da se dokument ucita u celosti pa onda upotrebi skriptu
            $(document).ready(function () {
                //sada pozivamo ajax funkciju za fetching data
                $.ajax({
                    url: 'dbconnection.php',
                    type: 'POST',
                    success: function (result_holder) {
                        //ukoliko je rezultat razlicit od errora koji se na njemu pojavljuju onda izvrsi
                        //tj.ukoliko nema gresaka
                        if (!result_holder.error) {
                            $('#greetings').html(result_holder);
                        }
                    }
                });
            });

        </script>
    </body>
</html>