<!--https://api.jquery.com/jquery.post/-->
<div class="container">
    <h2>Insert data form</h2>
    <form method="post" class="form-horizontal" id="insertdata" role="form" action="processajax.php">
        <?php // foreach ($results as $result):    ?>
        <div class="form-group">
            <label for="email">Email:</label>
            <input value="<?php //$result['email'];                  ?>" type="email" class="form-control" id="email" placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input value="<?php //$result['password'];                  ?>" type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
        </div>
        <div class="form-group">
            <label for="full_name">Full Name:</label>
            <input value="<?php //$result['full_name'];                 ?>" type="text" class="form-control" id="full_name" placeholder="Enter Full Name" name="full_name">
        </div>
        <button name="submit" value="submit" type="submit" class="btn btn-primary">Update user</button>
    </form>
</div>
<script>
    /*jQuery syntax is $(selector).serialize();
     * $.post(); u sebi sadrzi serialize metod
     First name: Mickey
     Last name: Mouse
     
     Serialize form values:
     //prebacuje u format koji je u skladu sa kupljenjem podataka
     metodom GET iz url adrese pa php moze da ih pokupi npr
     
     Forma podataka nakon serialize je:
     FirstName=Mickey&LastName=Mouse
     
     */
    //ajax syntaxa $.ajax({});
    //jquery syntax $().ready-ili-druga-funkcija();
    //kombinacija jquery i ajax $(document).ready-ili-druga-funkcija(function(){$.ajax({});});
    $(document).ready(function () {
        //featching data by ajax $.ajax({});
        $.ajax({
            url: 'processajax.php',
            type: 'POST',
            //ukoliko je uspesan proradice funkcija
            success: function (holderesults) {
                //ukoliko nema gresaka !holderesult.error
                if (!holderesults.error) {
                    $('#greetings').html(holderesults)
                }
            }
        });
    });
</script>
<script>
    //$.post(); SYNTAX AND PARAMETERS za insert podataka u bazu
    //
    //$.post(); data type will be automaticly set to type POST
    //$.post(url, data, function(data){$(".result").html(data);});
    //kada se dokument ucita izvrsava se sledeca funkcija
    $(document).ready(function () {
        //definisacemo sta se desava kada kliknemo na submit iz forme preko id iz html-a i jquery-ja uz pomoc jquery funkcije submit();
        $('#insertdata').submit(function (dontrefreash) {
            //(function(dontrefreash) { --- sada smo dali ovoj funkciji ime dontrefreash
            //zelimo da insertujemo podatke u bazu bez refresh stranice koji se automatski desava na klik button submit
            //da bismo sprecili taj refreash koji je po defaultu definisan da se desava na klik koristimo js funkciju preventDefault();
            dontrefreash.preventDefault();
            //    $(this) -- this znaci u ovom slucaju da url varijabla ima vrednost
//sa ove stranice dakle url varijabla na koju se ovde misli je dobila vrednost na ovoj stranici
//attr() je atribut iz html-a
//ovo znaci da ce se u var url staviti vrednost iz atributa action u html-u na ovoj stranici zbog this
            var url = $(this).attr("action");
            //moramo pokupiti podatke data, 
            //kada kliknemo na submit data ce biti poslata
            var data = $(this).serialize();
            //insert data by ajax $.post(url, fuction on success);
//definisacemo u $.post kao jedan od parametara funkciju koju cemo nazvati resetform
//zato sto hocemo da se nakon submitovanja forme ajaxom i jquerijem ociste input polja bez refresovanja
            $.post(url, data, function(resetform){
                $("#indrtdata")[0].reset();
            });
            
        });
    });
</script>

