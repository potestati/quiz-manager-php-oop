<?php

/*
 * construct metod
php ima mogucnost da izvrsi metodu automatski bez pozivanja
 * destruct metod
 * takodje php ima mogucnost da ubije funkciju bez pozivanja
 */

Class Cars {

//properties
    public $name = "BMW";
    static public $speed = 400;
    public $user = "Curry";
    public $type = "4WD";
    static private $minpasswordlengt = 8;

//Methods
    function desc() {
        echo 'My car is ' . $this->name . "KMPH and it is a " . $this->type . ".";
    }

    static public function checkspeed($check_speed) {
        if ($check_speed <= self::$speed) {
            echo 'Your car is very fast ';
        } else {
            echo 'Your car is not that fast ';
        }
    }
    //construct function nema ime funkcije
    //razlog za ovo je sto se izvrsava automatski kada se pozove ta klasa
    //ona konstruise klasu
    
    public function __construct(){
        echo 'hi ' . $this->user . ' You are welcome<br> ';
    }
    //Destruct metod nemaju imena
    public function __destruct() {
        echo '<br>Goodbye ' . $this->name;
    }
}

class Futurcars extends Cars {

    public $type = "Trucks";
    public $name = "Ford";

    function dexcfuturcar() {
        parent::desc();
    }

}
//kada napravimo instancu klase
//constract metod se automatski poziva
$myfirstCar = new Cars;

$check_speed = 300;
Cars::checkspeed($check_speed);
$myfutureCars = new Futurcars;
$myfutureCars->dexcfuturcar();
