<?php

/* 
 * POSTOJI PDO KLASA
 * PDO Statement klasa ako nema gresaka
 * PDO Exception klasa ako ima gresaka
 * PDO Drivers 
 * https://www.php.net/manual/en/pdo.construct.php
 * 
 *PDO - OOP DATABASES FOR PHP
 * PDO klasa
 * iz te klase se pozivaju potrebni metodi
 * kada se PDO klasa instancira prvo sto se izvrsava je __construct
 * ova funkcija ima parametre za konekciju na bazu
 * public PDO::__construct ( 
 * string $dsn [, string $username [, string $passwd [, array $options ]]] 
 * )
 * 
 * Parameters ¶
dsn - DATA source name je tip baze podataka
 * $dsn = 'mysql:dbname=testdb;host=127.0.0.1';
 *         tip sql jezika za bazu:dbname=ime_baze;host=ip adresa hosta
The Data Source Name, or DSN, contains the information required to connect to the database.

In general, a DSN consists of the PDO driver name, followed by a colon, followed by the PDO driver-specific connection syntax. Further information is available from the PDO driver-specific documentation.

The dsn parameter supports three different methods of specifying the arguments required to create a database connection:

Driver invocation
dsn contains the full DSN.

URI invocation
dsn consists of uri: followed by a URI that defines the location of a file containing the DSN string. The URI can specify a local file or a remote URL.

uri:file:///path/to/dsnfile

Aliasing
dsn consists of a name name that maps to pdo.dsn.name in php.ini defining the DSN string.

Note:

The alias must be defined in php.ini, and not .htaccess or httpd.conf

username
The user name for the DSN string. This parameter is optional for some PDO drivers.

passwd
The password for the DSN string. This parameter is optional for some PDO drivers.

options
A key=>value array of driver-specific connection options.

Return Values ¶
Returns a PDO object on success.

Errors/Exceptions ¶
PDO::__construct() throws a PDOException if the attempt to connect to the requested database fails.
 */

/*
Examples ¶
Example #1 Create a PDO instance via driver invocation
$connection = new PDO($dsn, $user, $password, $options);
 * 
 * $options - je tip konekcije da li je stalna ako jeste onda true , option je array
 * example
 * $option =  array(
 *  PDO::ATTR_PERSISTENT => true;
 *  PDO::ATTR_ERROR => PDO::ERRMODE_EXCEPTION;
 * )
*/
/* Connect to a MySQL database using driver invocation */
$dsn = 'mysql:dbname=testdb;host=127.0.0.1';
$user = 'dbuser';
$password = 'dbpass';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

/*https://www.php.net/manual/en/book.pdo.php
 * 
 * opis metoda pdo klase

 *  */

//prepere vraca string i koristi se kada se pristupa bazi kako bi se dobio 
//query , u proceduralnom nesto slicno radi 

/*
Please note this:

Won't work:
$sth = $dbh->prepare('SELECT name, colour, calories FROM ?  WHERE calories < ?');

THIS WORKS!
$sth = $dbh->prepare('SELECT name, colour, calories FROM fruit WHERE calories < ?');

The parameter cannot be applied on table names!!
 * 
 * slicna funkciji mysqli_query($connection, $query);
 * 
 * https://www.w3schools.com/php/php_mysql_prepared_statements.asp
 * 
 * $servername = "localhost";
$username = "username";
$password = "password";
$dbname = "myDB";
*/
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// prepare and bind
//PDO::prepere()vraca true ako nema gresaka vraca PDOStatement object a ako ima onda
//PDOException ako je false
$stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) VALUES (?, ?, ?)");
$stmt->bind_param("sss", $firstname, $lastname, $email);

// set parameters and execute
$firstname = "John";
$lastname = "Doe";
$email = "john@example.com";
$stmt->execute();

$firstname = "Mary";
$lastname = "Moe";
$email = "mary@example.com";
$stmt->execute();

$firstname = "Julie";
$lastname = "Dooley";
$email = "julie@example.com";
$stmt->execute();

echo "New records created successfully";

$stmt->close();
$conn->close();


/*Prepere funkcija cuva bazu podataka

 * ukkoliko ne zelimo da pristupimo bazi preko prepere nego direktno
 * onda koristimo funkciju koja direktno radi kao mysqli_query
 * to je query method
 */
$sql = 'SELECT name, color, calories FROM fruit ORDER BY name';
foreach ($conn->query($sql) as $row) {
    print $row['name'] . "\t";
    print $row['color'] . "\t";
    print $row['calories'] . "\n";
}
/*
 * output
 * apple   red     150
banana  yellow  250
kiwi    brown   75
lemon   yellow  25
orange  orange  300
pear    green   150
watermelon      pink    90
 *  */

/* LAST INSERTED ID */
lastInsertId();

$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "myDBPDO";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO MyGuests (firstname, lastname, email)
    VALUES ('John', 'Doe', 'john@example.com')";
    // use exec() because no results are returned
    $conn->exec($sql);
    $last_id = $conn->lastInsertId();
    echo "New record created successfully. Last inserted ID is: " . $last_id;
    }
catch(PDOException $e)
    {
    echo $sql . "<br>" . $e->getMessage();
    }

$conn = null;
