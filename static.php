<?php
//https://code.tutsplus.com/tutorials/as3-101-quick-tip-when-to-use-static-properties-and-methods--active-7832
/*
  Nema static Class klase nego je static properti ili metod
 * static private $name;
 */

Class Cars {

//properties
    static private $name = "BMW";
    static public $speed = 400;
    private $user = "Curry";
    private $type = "4WD";
    
    static private $minpasswordlengt = 8;

//Methods
    //ako se properti proglase static onda metod da bi mogao da im pristupi 
    //mora da bude static
    //scope resolution operator ::
//    function desc() {
//        echo 'My car is ' . $this->name . 'and it runs at ' . $this->speed . "KMPH and it is a " . $this->type . ".";
//    }

//akoo u funkciji pozivamo static property onda je self::$speed
//This is the basic requirement. 
//If a property needs to have independent values across multiple instances, 
//then the property can’t be static.
//ukoliko zelimo da nasa vrednost bude ne promenjena kroz aplikaciju 
//kao npr minimalna duzina password-a nesto sto se nece menjati kroz aplikaciju    
//onda cemo staviti static private pass_length
    static public function checkspeed($check_speed) {

        if ($check_speed <= self::$speed) {
            echo 'Your car is very fast';
        } else {
            echo 'Your car is not that fast';
        }
    }

}

$myfirstCar = new Cars;
//kroz metod dobijamo pristup propertima
//$myfirstCar->desc();
//ako direktno pristupamo propertiju a private je dobijamo error
//$myfirstCar->speed;

//u klasi se stavi zasticen properti a onda se definise
//metod preko kojeg pristupamo propertima

//pozivanje static metoda
$check_speed = 300;
Cars::checkspeed($check_speed);
