<!DOCTYPE html>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/main.css">

<!------ Include the above in your HEAD tag ---------->

<section id="tabs" class="project-tab">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Project Tab 1</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Project Tab 2</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Project Tab 3</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Full Name</th>
                                    <th>Password</th>
                                    <th>Email</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                include('pdocon.php');
                                $db = new Pdocon;
                                $db->query("SELECT * FROM users");
                                $results = $db->fetchMultiple();
//                               echo gettype($result);
                                foreach ($results as $result) :
                                    ?>
                                    <tr>
                                        <td><a href="#"><?php echo $result['full_name']; ?></a></td>
                                        <td><?php echo $result['password']; ?></td>
                                        <td><?php echo $result['email']; ?></td>
                                        <td><a class="btn btn-primary" href="update.php?user_id=<?php echo $result['id']; ?>">Edit</a></td>
                                    </tr>
                                <?php endforeach; ?>  
                            </tbody>
                        </table>
                    </div>
                    <?php
                    
                    //UNOS PODATAKA EDIT I BINDING OF VALUES IN TO THE DATABASE
                    
                    $db->query("INSERT INTO users(id, email, password, full_name)VALUES(null, :email, :password, :full_name)");
                    //VALUES(null, :email, :password, :full_name)"); -- sada unosimo vrednosti u bazu a ovaj nacin unosa se zove
                    //bindovanje ( bajdovanje) a :password i :full_name su placeholderi polja u bazi tj kljucevi za ta mesta
//                    $db->bindValue($parm, $value, $type);
                    //podesi bajdovanje
                    $db->bindValue(':full_name', 'Test Test', PDO::PARAM_STR);
                    $db->bindValue(':email', 'test@gmail.com', PDO::PARAM_STR);
                    $db->bindValue(':password', 'test123', PDO::PARAM_STR);
                    //izvrsi bajdovanje
                    $db->execute();
                    $db->confirm_result();
                    ?>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Employer</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                echo 'hello';
                                include('pdocon.php');
                                $db = new Pdocon;
                                var_dump($db);
                                $db->query("SELECT * FROM users");
                                $result = $db->fetchMultiple();
                                ?>
                                <tr>
                                    <td><a href="#"><?php echo $result['full_name']; ?></a></td>
                                    <td><?php echo $result['password']; ?></td>
                                    <td><?php echo $result['email']; ?></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <table class="table" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Contest Name</th>
                                    <th>Date</th>
                                    <th>Award Position</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="#">Work 1</a></td>
                                    <td>Doe</td>
                                    <td>john@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 2</a></td>
                                    <td>Moe</td>
                                    <td>mary@example.com</td>
                                </tr>
                                <tr>
                                    <td><a href="#">Work 3</a></td>
                                    <td>Dooley</td>
                                    <td>july@example.com</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
