<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!------ Include the above in your HEAD tag ---------->
    </head>
    <body>

        <!--<section id="tabs" class="project-tab">-->
        <div class="container">
            <h2>Insert data form</h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <?php // foreach ($results as $result):  ?>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input value="<?php //$result['email'];            ?>" type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input value="<?php //$result['password'];            ?>" type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
                </div>
                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <input value="<?php //$result['full_name'];           ?>" type="text" class="form-control" id="full_name" placeholder="Enter Full Name" name="full_name">
                </div>
                <button name="submit" type="submit" class="btn btn-primary">Update user</button>
            </form>
        </div>
        <?php
        include('pdocon.php');
        $db = new Pdocon;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (!(empty($_POST['full_name']))) {
                $row_name = trim($_POST['full_name']);
                $c_name = filter_var($row_name, FILTER_SANITIZE_STRING);
            } else {
                var_dump($_POST['full_name']);
            }
            if (!(empty($_POST['email']))) {
                $row_email = trim($_POST['email']);
                $c_email = filter_var($row_email, FILTER_SANITIZE_STRING);
            } else {
                echo 'email is empty';
            }
            if (!(empty($_POST['pwd']))) {
                $row_pswd = trim($_POST['pwd']);
                $c_pswd = filter_var($row_pswd, FILTER_SANITIZE_STRING);
            } else {
                echo 'pwd is empty';
            }

            if (isset($_POST['submit'])) {
                $sql = 'INSERT INTO `users` (`email`, `pwd`, `full_name`) VALUES (:email, :password, :full_name)';
                var_dump($_POST['password']);
                $stmt = $db->query($sql);
                var_dump($stmt);//ovde dobijem null
                $stmt->bindValue(':full_name', $_POST['full_name'], PDO::PARAM_STR);
                $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                $stmt->bindValue(':password', $_POST['pwd'], PDO::PARAM_STR);

                $run = $stmt->execute();

                if ($run) {
                    header("Location: edit-table.php");
                }
            }
        }
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <table class="table" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Full Name</th>
                                        <th>Password</th>
                                        <th>Email</th>
                                        <th>Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $db->query("SELECT * FROM users");
                                    $results = $db->fetchMultiple();
//                               echo gettype($result);
                                    foreach ($results as $result) :
                                        ?>
                                        <tr>
                                            <td><a href="#"><?php echo $result['full_name']; ?></a></td>
                                            <td><?php echo $result['password']; ?></td>
                                            <td><?php echo $result['email']; ?></td>
                                            <td><a class="btn btn-primary" href="update.php?user_id=<?php echo $result['id']; ?>">Edit</a></td>
                                        </tr>
                                    <?php endforeach; ?>  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>