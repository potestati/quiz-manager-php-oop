<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Edit user</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <?php
        include('../pdocon.php');
        $db = new Pdocon;

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
//            if (empty($name)) {
//                echo 'Name is empty';
//            } else {
//                echo 'Your name is ' . $email;
//            }
//            
            //collect value of input field name
            $row_name = trim($_POST['full_name']);
            $row_email = trim($_POST['email']);
            $row_pswd = trim($_POST['pswd']);
            $row_check = trim($_POST['remember']);

            //validating
            echo $c_name = filter_var($row_name, FILTER_SANITIZE_STRING);
            echo $c_email = filter_var($row_email, FILTER_VALIDATE_EMAIL);
            echo $c_pswd = filter_var($row_pswd, FILTER_SANITIZE_STRING);
            echo $c_check = filter_var($row_check, FILTER_SANITIZE_STRING);

            //ukoliko je setovano bindujemo vrednosti
            if (isset($_POST['submit'])) {// u zagradi ispred koje pise insert into user su polja iz baze
                $db->query("INSERT INTO users (id, full_name, email, password) VALUES (NULL, :full_name, :email, :password )");
                $db->bindvalue(':full_name', $c_name, PDO::PARAM_STR);
                $db->bindvalue(':email', $c_email, PDO::PARAM_STR);
                $db->bindvalue(':password', $c_pswd, PDO::PARAM_STR);
                $run = $db->execute();

                if ($run) {
                    echo 'You have successfully inserted your values';
                } else {
                    echo 'Somethings goes wrong';
                }
            }
        }
        ?>
        <div class="container">
            <h2>Stacked form</h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                </div>
                <div class="form-group">
                    <label for="full_name">Full Name:</label>
                    <input type="text" class="form-control" id="full_name" placeholder="Enter Full Name" name="full_name">
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="remember" value=1> Remember me
                    </label>
                </div>
                <button name="submit" type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>
