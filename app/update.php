<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Update user</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
        <!--
        BINDOVANJE VREDNOSTI U BAZU OBJASNJENJE
        
        $db->query("UPDATE users SET email=:email, password=:password, full_name=:fullname WHERE id=:userid");
        $db->bindValue(':fullname', $c_name, PDO::PARAM_STR);
        $db->bindValue(':email', $c_email, PDO::PARAM_STR);
        $db->bindValue(':password', $c_pswd, PDO::PARAM_STR);
        -->
        <?php
        if (isset($_GET['user_id'])) {
            $user_id = $_GET['user_id'];
        }

        include('pdocon.php');
        $db = new Pdocon;

        $db->query('SELECT * FROM users WHERE id= :userid');
        $db->bindValue(':userid', $user_id, PDO::PARAM_INT);
        //ova funkcija fetchMultiple dolazi sa execute u sebi
        $results = $db->fetchMultiple();
        ?>
        <div class="container">
            <h2>Registration form</h2>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                <?php foreach ($results as $result): ?>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input value="<?php $result['email']; ?>" type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input value="<?php $result['password']; ?>" type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd">
                    </div>
                    <div class="form-group">
                        <label for="full_name">Full Name:</label>
                        <input value="<?php $result['full_name']; ?>" type="text" class="form-control" id="full_name" placeholder="Enter Full Name" name="full_name">
                    </div>
                    <!--                    <div class="form-group form-check">
                                            <label class="form-check-label">
                                                <input value="" class="form-check-input" type="checkbox" name="remember" value=1> Remember me
                                            </label>
                                        </div>-->
                <?php endforeach; ?>
                <button name="update_user" type="submit" class="btn btn-primary">Update user</button>
            </form>
        </div>
        <?php
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
//            if (empty($name)) {
//                echo 'Name is empty';
//            } else {
//                echo 'Your name is ' . $email;
//            }
//            
            //collect value of input field name
            $row_name = trim($_POST['full_name']);
            $row_email = trim($_POST['email']);
            $row_pswd = trim($_POST['pswd']);
//            $row_check = trim($_POST['remember']);
            //validating
            $c_name = filter_var($row_name, FILTER_SANITIZE_STRING);
            $c_email = filter_var($row_email, FILTER_VALIDATE_EMAIL);
            $c_pswd = filter_var($row_pswd, FILTER_SANITIZE_STRING);
//            $c_check = filter_var($row_check, FILTER_SANITIZE_STRING);

            if (isset($_POST['update_user'])) {
                $db->query("UPDATE users SET email=:email, password=:password, full_name=:fullname WHERE id=:userid");
                $db->bindValue(':fullname', $c_name, PDO::PARAM_STR);
                $db->bindValue(':email', $c_email, PDO::PARAM_STR);
                $db->bindValue(':password', $c_pswd, PDO::PARAM_STR);
                $db->bindValue(':userid', $user_id, PDO::PARAM_INT);

                $run = $db->execute();
                
                if($run){
                    header("Location: edit-table.php");
                }
            }
        }
        ?>

    </body>
</html>
