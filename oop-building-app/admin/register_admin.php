<?php
include('includes/header.php');
//Include functions
include 'includes/functions.php';

//check to see if user if logged in else redirect to index page 
?>
<!--ukoliko ce forma da procesuira image onda nam 
treba type file i u tagu forme mora da stoji atribut  
enctype="multipart/form-data"-->

<?php
/* * ************ Register new Admin ***************** */

//require database class files
require 'includes/pdocon.php';
/* WHAT WE DOING HERE */

//Hash password using our md5 function
//Check and see if user already exist in database using email so write query and bind email   
//Call function to count row
//Display error if admin exist 
//Write query to insert values, bind values
//Execute and assign a varaible to the execution result // remember it returns true of false
//Comfirm execute and display error or success message
//instatiating our database objects

$db = new Pdocon;

//Collect and clean values from the form // Collect image and move image to upload_image folder
if (isset($_POST['submit_login'])) {
    //clean data
    $row_name = cleandata($_POST['name']);
    $row_sex = cleandata($_POST['sex']);
    $row_email = cleandata($_POST['username']);
    $row_password = cleandata($_POST['password']);
//prepere for database
    $c_name = sanitizestr($row_name);
    $c_sex = sanitizestr($row_sex);
    $c_email = valemail($row_email);

    //clean password
    $c_password = sanitizestr($row_password);

    //hash password
    $hashed_Pass = hashpassword($c_password);

    //collect image
    //treba nam global $_FILES , prvo pokupi name fajla pa onda rutu do fajla
    
//globals FILES trazi ['image'] iz forme name:image i [name] to je ime samog fajla
    $c_img = $_FILES['image']['name'];
    //ova slika mora da bude sacuvana temporarly pre nego sto je pomerimo na stalnu poziciju
    //u donjem redu cemo sacuvati tip image i privremeni naziv fajla

    $img_tmp = $_FILES['image']['tmp_name'];

    //sada treba da pomerimo sliku u stalan folder
    //move image to the permanent location
    move_uploaded_file($img_tmp, "uploaded_image/$c_img");
    $placeholder_image = "placeholder-image.jpg"; // umasto dodavanje prave slike, neka ide u bazu uvek "placeholder-image.jpg" do daljnjeg
//proveravamo da li vec postoji dati username tj email u bazi
    $db->query("SELECT * FROM admin WHERE email= :email");
    $db->bindvalue(':email', $c_email, PDO::PARAM_STR);
    $row = $db->fetchSingle();

    if ($row) {
        echo '<div class="alert alert-danger">
  <strong>Danger!</strong> User already exist. Register or try again.
</div>';
    } else {
        $db->query("INSERT INTO admin(id, fullname, email, password, sex, image) VALUES(NULL, :fullname, :email, :password, :sex, :image)");
        $db->bindvalue('fullname', $c_name, PDO::PARAM_STR);
        $db->bindvalue('email', $c_email, PDO::PARAM_STR);
        $db->bindvalue('password', $c_password, PDO::PARAM_STR);
        $db->bindvalue('sex', $c_sex, PDO::PARAM_STR);
        $db->bindvalue('image', $placeholder_image, PDO::PARAM_STR);
        $run = $db->execute();

        if ($run) {
            echo '<div class="alert alert-success text-center">
  <strong>Success!</strong> Admin registered successfully. Please login.
</div>';
        } else {
            echo '<div class="alert alert-danger text-center">
  <strong>Sorry!</strong> Admin user could not be registered. Please try again leter.
</div>';
        }
    }
}
?>
<!--jovana  0641506030--> 

<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <p class=""><a class="pull-right" href="../index.php"> Login</a></p><br>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <form class="form-horizontal" role="form" method="post" action="register_admin.php" enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label col-sm-2" for="name"></label>
                <div class="col-sm-10">
                    <input type="name" name="name" class="form-control" id="name" placeholder="Enter Full Name" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="sex"></label>
                <div class="col-sm-10">
                    <select type="" name="sex" class="form-control" id="sex" >
                        <option value="">Select Sex</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Secret">N/A</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-2" for="email"></label>
                <div class="col-sm-10">
                    <input type="email" name="username" class="form-control" id="email" placeholder="Enter Email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="pwd"></label>
                <div class="col-sm-10"> 
                    <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter Password" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="image"></label>
                <div class="col-sm-10">
                    <!--ukoliko ce forma da procesuira image onda nam treba type file i u tagu forme mora da stoji atribut  enctype="multipart/form-data"-->
                    <input type="file" name="image" id="image" placeholder="Choose Image">
                </div>
            </div>

            <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <label><input type="checkbox" required> Accept Agreement</label>
                    </div>
                </div>
            </div>

            <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10 text-center">
                    <button type="submit" class="btn btn-primary pull-right" name="submit_login">Register</button>
                    <a class="pull-left btn btn-danger" href="../index.php"> Cancel</a>
                </div>
            </div>
        </form>

    </div>
</div>

</div>
</div>

<?php include('includes/footer.php'); ?>  