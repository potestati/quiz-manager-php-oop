<?php

/*
  Here we will create helper function which we can use again
 *  */

//function to trim values
function cleandata($value) {
    return trim($value);
}

//function to sanitize value for string
function sanitizestr($row_value) {
    return filter_var($row_value, FILTER_SANITIZE_STRING);
}

//function to validate value for email
function valemail($row_email) {
    return filter_var($row_email, FILTER_VALIDATE_EMAIL);
}

//function to validate value for integer
function valsint($row_int) {
    return filter_var($row_int, FILTER_SANITIZE_NUMBER_INT);
}

//function to redirect
function redirect($page) {
    header("Location: {$page}");
}

//function to keep error and success messages in a session 
//cuvacemo ove poruke u sesijama
function keepmsg($message) {

    if (empty($message)) {
        $message = "";
    } else {
        $_SESSION['msg'] = $message;
    }
}

//function to display the stored message in the session super global
function showmsg(){
    //isset() znaci if it is ready
    if(isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
}
//Create function to hash password using md5
function hashpassword($pass){
    return md5($pass);
}



