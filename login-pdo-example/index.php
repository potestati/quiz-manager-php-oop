<?php
session_start();
$host = "localhost";
$username = "root";
$password = "";
$database = "testing";
$message = "";

try {
    $connect = new PDO("mysql:host=$host;dbname=$database", $username, $password);
    //da bi se hendlovao Exception od strane php-a ovaj red koda je potreban
    //da bi se resio handle exception mora se setovati atribut
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//provericemo da li je login dugme submitovano ili ne
    if (isset($_POST["login"])) {
        if (empty($_POST["login"]) || empty($_POST["password"])) {
            $message = '<label>All fields are required.</label>';
        } else {
            $query = "SELECT * FROM users WHERE username = :username AND password = :password";
            $statement = $connect->prepare($query);
            //execute() function znaci izvrsenje querija
            $statement->execute(array(
                'username' => $_POST["username"],
                'password' => $_POST["password"],
            ));
            //sada hocu da prebrojim koliko je rezultata nasao kada je izvrsio query
            $count = $statement->rowCount();
//            rowCount(); funkcija pokazuje sa koliko je redova tabele bilo pogodjeno izvrsenjem querija
            if($count>0){
                //ukoliko je taj broj veci od nula taj user moze da udje u sistem
                //setijemo sesiju naziva username a vrednost u njoj ce biti ono sto je postovano u formu
                //u input polje cije je ime username
                $_SESSION["username"] = $_POST["username"];
                //sistem tog usera redirektuje na drugu stranicu pa ce se na toj 
                //drugoj stranici iz sesije vaditi podatak da se proveri da li je to taj user
                header("location:login_success.php");
            } else {
                $message = '<label>Wrong data there is no user.</label>';
            }
        }
    }
    //PDOException $error napravljena je instanca objekta PDOException u var $error
    //ako postoji greska bice u varijabli $error
} catch (PDOException $error) {
    $message = $error->getMessage();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>LOGIN PDO EXAMPLE with session</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        <br>
        <?php 
        //ukoliko postoji greska sacuvana u sesiji prilikom logovanja ovde ce se ispisati
        if(isset($message)){
            echo '<label class="text-danger">'.$message.'</label>';
        }
        ?>
        <div class="container" style="margin-top: 10%">
            <form method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">username</label>
                    <input name="username" type="text" class="form-control" placeholder="username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <input type="submit" name="login" value="login" class="btn btn-info">
            </form>
        </div>
    </body>
</html>
