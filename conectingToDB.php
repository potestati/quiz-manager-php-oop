<?php

/* 
 * 
 */
// one method
$connection = mysqli_connect($host, $user, $password, $database, $port, $socket);
/*$host - name of the server = localhost or ip adress
 * $user - this is username of the server = root if it is not server
 * if it is live it haave to be seted
 * $password - if it is set if it is live pass have to be set
 * $database - name of the database
 * last two parametars are not imported
 */
//kaze ukoliko imas odgovor od ove funkcije tj ukoliko nije null
if($connection){
    echo "Database connection is successfull" ;
} else {
    echo "Database connection is established " . mysqli_errno($connection);
    die($connection);
}

//--------------------------------------------
//second method
//ovde kaze try to connect mysqli_connect je connect
try{
    $connection = mysqli_connect($host, $user, $password, $database, $port, $socket);
//    throw new Exception("Some error message");
}catch(PDOException $e){
    echo $e->getMessage();
}

//instert in to database values
include('dbconnection.php'); //ukljucicemo konekciju u savki fajl koji je zahteva

$query = "INSERT INTO users(id, email,full_name)VALUES('NULL','potestati@gmail.com', 'Dragana Poznan')";
$run_query = mysqli_query($connection, $query); //var $connection je includovana iz drugog fajla a ovde je gore definisano

if($run_query){
    echo 'connectin is establish data hase been inserted';
} else {
    echo 'something goes wrong';
}

mysqli_close($connection);

//update data in database

include('dbconnection.php'); //ukljucicemo konekciju u savki fajl koji je zahteva

$query_update = "UPDATE users SET full_name = 'nova vrednost',email = 'test@gmiail.com' WHERE id = 9";
$run_query = mysqli_query($connection, $query_update); //var $connection je includovana iz drugog fajla a ovde je gore definisano

if($run_query){
    echo 'connectin is establish data has been updated';
} else {
    echo 'something goes wrong';
}

mysqli_close($connection);

//---------------------------------------------

//SELECT OR FETCHING DATA AND DELETE DATA

//select function
$query_select = "SELECT * FROM users";
$run_query = mysqli_query($connection, $query_select);

$result = mysqli_fetch_array($run_query, MYSQLI_NUM);
/*
 * MYSQLI_NUM is a constant in PHP associated with 
 * a mysqli_result. If you're using mysqli to 
 * retrieve information from the database, 
 * MYSQLI_NUM can be used to specify the return 
 * format of the data. Specifically, when using 
 * the fetch_array function, MYSQLI_NUM specifies 
 * that the return array should use numeric keys 
 * for the array, instead of creating an 
 * associative array.
 * MYSQLI_ASSOC - vraca rezultat asocijativnog niza
 * ako hocemo da selektujemo sve onda
 * ONDA MORAMO DA KORISTIMO PETLJU DA PRODJEMO KROZ SVE PODATKE
 */
//fecujemo uzimamo jedan rezultat ili selektujemo
if($result){
    echo $result[0]; // output will be vrednost pod indexom 0
}else{
    echo 'Data could not be updated';
}

//DELETE
$query_delete = "DELETE FROM users WHERE id = 12";
$run_query = mysqli_query($connection, $query_delete);

if($run_query){
    echo 'data has been deleted';
}else{
    echo 'Data could not be delete';
}
//posle rada sa bazom zatvoriti konekciju
mysqli_close($connection);

//Looping through your results and Displaying it
//query
$query_select = "SELECT * FROM users";
//unos querija u bazu i stavljanje rezultaata u var
$run_query = mysqli_query($connection, $query_select);
//SADA TREBA DA TE PODATEKE PREBACIMO U NIZ RADI MANIPULACIJE
//
//vraca asocijativni niz
//$result = mysqli_fetch_assoc($run_query);

//vraca indexiran niz podataka
$result = mysqli_fetch_row($run_query);

//TREBA DA PRODJEMO SADA KROZ PODATKE IZ BAZE KOJI SU SADA
//STAVLJENI U NIZ
//fetch assoc vraca asocijativni niz
while($result = mysqli_fetch_assoc($result)){
    echo $result['email'];
}


//--------------------------------------------
//ukljucivanje fajla sa konekcijom
include 'file-with-connection.php';
//definisanje varijabli
$email = "test@gmail.com";
$password = "0000";
//definisanje sql jezikom sta hocemo i to stavimo u var
//selektuj sve iz tabele users gde je email taj koji je u varijabli i password taj koji je u varijabli
//email i password su takodje polja u bazi
$query_select = "SELECT * FROM users WHERE email = '$email' and password = '$password'";
//pokrecemo konekciju sa funkcijom mysquli_query 
$run_query = mysqli_query($connection, $query_select);
//sada te podatke traba pretvoriti u niz
//num rows vraca broj redova u tabeli
$result = mysqli_num_rows($run_query);
//sada dobijamo broj koliko ima redova u toj tabeli
echo $result;

