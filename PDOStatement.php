<?php

/* 
 * PDOStatement klasa ukoliko je uspesna konekcija
 * vraca se objekat nakon pdo construct
 * tako da mozemo koristiti metode iz PDOStatement objekta
 * 
 */

//public function bindValue($parm, $value, $type){
//  takes two arguments and a type
//  //ovo je bind proceduralno
//  $query = SELECT * FROM users WHERE id = $id AND email = $email
//  
//  u pdo ide prvo prepare();
//  i onda radimo bind vrednosti
//  
//  bind Value i execute izvrsavaju neku radnju na bazi
//  dok recimo prepare() metod samo priprema podatke i vraca string
//  proverava samo da li je konekcija uspostavljena i kuca na vrata
//  baze podataka
//  
//  za sve radnje koristimo bind value i execute funciju
//}

//bindValue()
//def parametre
$dsn = 'mysql:dbname=testdb;host=127.0.0.1';
$user = 'dbuser';
$password = 'dbpass';
//pozivanje pdo klase i unos potrebnih parametara za konekciju
try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
//uspeh vraca se objekat PDOStatement u var $dbh
$dbh->prepare("SELECT * FROM users WHERE id=:id AND email=:email");
//id=:id ---- polje u tabeli id je jednako placeholderu id tj.vrednosti iz var $id

//public function bindValue($parm, $value, $type)
bindValue(":id", $id, PDO::PARAM_INT);
//PDO::PARAM_STR -- je type string npr
//PDO::PARAM_INT -- tip je integer
//bindValue("polje_u_bazi", $id vrednostIzVarjable, tip vrednosti PDO::PARAM_INT);
//Example
/* Execute a prepared statement by binding PHP variables */
//varijable dobijaju vrednost nekako
$calories = 150;
$colour = 'red';

//pripremicemo podatke iz baze u var sth dbh je PDO __construct

$sth = $dbh->prepare('SELECT name, colour, calories
    FROM fruit
    WHERE calories < :calories AND colour = :colour');
//$sth->bindValue(placeholder u bazi, vrednost iz var, tip podatka);
$sth->bindValue(':calories', $calories, PDO::PARAM_INT);
$sth->bindValue(':colour', $colour, PDO::PARAM_STR);
$sth->execute();
//Execute()


/* public function execute(){
 *  go ahead and execute the query with the binded values and parameters
 * }
 *The execution returns true successfull and false when not
 */

/*fetchAll();
 * ovo je PDOStatement metod koji je u proceduralnom slican
 * mysqli_fetch_assoc();
 * fetch all rows in an Assoc array

 *  */
/*fetch();
//fetch a single row in Assoc array - mysqli_fetch_row();

 *  */

/*
rowCount();
 * number of rows in the result set from the executed method
 * mysqli_num_rows();
 * 
 *  */


