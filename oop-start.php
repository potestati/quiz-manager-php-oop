<?php

/*

 */

Class Primer {

    public $name = 'test';

    public function display() {
        return $this->name;
    }

}

Class Test extends Primer {

    public $lastname;

}

$primer = new Primer;
//pozivanje funkcije
echo $primer->display();
echo '<br>';
//pozivanje property iz klase
echo $primer->name;
echo '<br>';
//2.

Class Cars {
//properties
    private $name = "BMW";
    protected $speed = 400;
    private $user = "Curry";
    private $type = "4WD";

//Methods
    function desc(){
        echo 'My car is ' . $this->name . 'and it runs at ' . $this->speed . "KMPH and it is a " . $this->type . ".";
        
    }
    

}

$myfirstCar = new Cars;
//kroz metod dobijamo pristup propertima
$myfirstCar->desc();
//ako direktno pristupamo propertiju a private je dobijamo error
$myfirstCar->speed;

//u klasi se stavi zasticen properti a onda se definise
//metod preko kojeg pristupamo propertima


