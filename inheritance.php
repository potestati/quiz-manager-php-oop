<?php

/*

 */

Class Cars {

//properties
//ako stavimo da je private ne moze se menjati iz child clase taj parametar
//    private $name = "BMW";
    //ako stavimo da je protected moze se menjati iz chlid clase vrednost parametra
    protected $name = "BMW"; 
    static public $speed = 400;
    private $user = "Curry";
    public $type = "4WD";
    static private $minpasswordlengt = 8;

//Methods
    public function desc() {
        echo '<br>My car is ' . $this->name . " KMPH and it is a " . $this->type . ".";
    }

    static public function checkspeed($check_speed) {

        if ($check_speed <= self::$speed) {
            echo 'Your car is very fast';
        } else {
            echo 'Your car is not that fast';
        }
    }

}

class Futurcars extends Cars {

    //ako cemo u buducnosi da vozimo kombi
    //mozemo da promenimo type var iz roditeljske
    //ovo se zove overajdovanje
    public $type = "Trucks";
    //da bi se promenila vrednost parametra iz roditeljske klase tamo mora biti protected pa i ovde
    protected $name = "Ford";

    function dexcfuturcar() {
        //pozivanje metoda iz roditeljske klase u metodu u child klasi
//    desc je metod definisan u roditeljskoj klasi
        parent::desc();
    }

}

$myfirstCar = new Cars;
//kroz metod dobijamo pristup propertima
//$myfirstCar->desc();
//ako direktno pristupamo propertiju a private je dobijamo error
//$myfirstCar->speed;
//pozivanje static metoda
$check_speed = 300;
Cars::checkspeed($check_speed);
//pozivanje metoda koji poziva metod iz roditeljske klase
$myfutureCars = new Futurcars;
$myfutureCars->dexcfuturcar();
